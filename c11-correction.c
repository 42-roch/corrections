/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c11-correction.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/20 13:48:32 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/22 11:20:13 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "ex00/ft_foreach.c"
#include "ex01/ft_map.c"
#include "ex02/ft_any.c"
#include "ex03/ft_count_if.c"
#include "ex04/ft_is_sort.c"
#include "ex06/ft_sort_string_tab.c"
#include "ex07/ft_advanced_sort_string_tab.c"

/**
 * Gloabals variables
 **/
int	*g_zero;
int	*g_one;

void	ft_print_ex(int id, bool ok)
{
	printf("Exercice \033[0;33m#0%d\033[0;37m : ", id);
	if (ok)
		printf("\033[0;32mO.K.");
	else
		printf("\033[0;31mK.O.");
	printf("\033[0m\n");
}

void	zero(int a)
{
	g_zero[a] = a + 1;
}

int	one(int a)
{
	return (a + 5);
}

int	two(char *a)
{	
	if (a[0] == 'a')
		return (0);
	return (1);
}

int	three(char *a)
{
	if (a[0] == 'a')
		return (5);
	return (0);
}

int	four(int a, int b)
{
	if (a < b)
		return (-1);
	else if (a > b)
		return (1);
	return (0);
}

int seven(char *s1, char *s2)
{
	int i;

	i = 0;
	while (s1[i] == s2[i] && s1[i] != '\0' && s2[i] != '\0')
		i++;
	return (s1[i] - s2[i]);
}

int	main(void)
{
	/* Header */
	printf("\033[0;37m----------[\033[0;34mC11\033[0;37m]----------\n\n");
	printf("Developed by \033[0;34mRochB\033[0;37m.\n");
	printf("Version \033[0;34m1.0\n");
	printf("mmwww.roch-blondiaux.com\033[0;37m\n\n");

	/* Exercice 00 */
	int zeroArray[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	g_zero = zeroArray;
	ft_foreach(g_zero, 10, zero);
	ft_print_ex(0, g_zero[0] == 1 && g_zero[9] == 10 && g_zero[5] == 6);

	/* Exercice 01 */
	g_one = zeroArray;
	g_one = ft_map(g_one, 10, one);
	ft_print_ex(1, g_one[0] == 6 && g_one[9] == 15 && g_one[5] == 11);
	int f = 0;

	/* Exercice 02 */
	char **twoPointer;
	twoPointer  = malloc(500);
	twoPointer[0] = "b";
	twoPointer[1] = "abc";
	twoPointer[2] = "abcd";
	twoPointer[3] = "abcde";
	ft_print_ex(2, ft_any(twoPointer, two) == 1);

	/* Exercice 03 */
	ft_print_ex(3, ft_count_if(twoPointer, 4, three) == 3);
	free(twoPointer);

	/* Exercice 04 */
	int	fourArray[] = {0, 1, 2, 3, 4};
	int fourArrayB[] = {4, 3, 1, 2, 0};
	int *fourPointer = fourArray;
	int *fourPointerB = fourArrayB;
	ft_print_ex(4, ft_is_sort(fourPointer, 5, four) == 1 && ft_is_sort(fourPointerB, 5, four) == 0);

	/* Exercice 05 */
	printf("Exercice \033[0;33m#05\033[0;37m : \n");
	system("cd ex05 && make fclean");
	system("cd ex05 && make all");
	printf("\nExcepted result: -17\n");
	system("./ex05/do-op 7zer7nv782az8 '-' -+-++-+-24azeazeaz5");
	system("cd ex05 && make fclean");
	
	/* Exercice 06 */
	char **sixPointer;
	sixPointer = malloc(500);
	sixPointer[0] = "abcde";
	sixPointer[1] = "abbc";
	sixPointer[2] = "abb";
	sixPointer[3] = "aaa";
	sixPointer[4] = NULL;
	ft_sort_string_tab(sixPointer);
	ft_print_ex(6, strcmp(sixPointer[0], "aaa") == 0 && strcmp(sixPointer[3], "abcde") == 0 && strcmp(sixPointer[2], "abbc") == 0);
	free(sixPointer);

	/* Exercice 07 */
	char **sevenPointer;
	sevenPointer = malloc(500);
	sevenPointer[0] = "abcde";
	sevenPointer[1] = "abbc";
	sevenPointer[2] = "abb";
	sevenPointer[3] = "aaa";
	sevenPointer[4] = NULL;
	ft_advanced_sort_string_tab(sevenPointer, seven);
	ft_print_ex(7, strcmp(sixPointer[0], "aaa") == 0 && strcmp(sixPointer[3], "abcde") == 0 && strcmp(sixPointer[2], "abbc") == 0);
	free(sixPointer);
	/* Norminette */
	printf("\n\n\033[0;33mNorminette outputs:\033[0;37m\n\n");
	system("norminette -R CheckForbiddenSourceHeader */*.c");

	/* Footer */
	printf("\n\n----------[\033[0;34mC11\033[0;37m]----------\n");
	return (0);
}