#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "ex00/ft_list.h"
#include "ex00/ft_create_elem.c"
#include "ex01/ft_list_push_front.c"
#include "ex02/ft_list_size.c"
#include "ex03/ft_list_last.c"
#include "ex04/ft_list_push_back.c"
#include "ex05/ft_list_push_strs.c"
#include "ex06/ft_list_clear.c"
#include "ex07/ft_list_at.c"
#include "ex08/ft_list_reverse.c"

t_list	*ft_create_obj(void *data, t_list *next)
{
	t_list	*obj;

	obj = NULL;
	obj = malloc(sizeof(t_list));
	obj->data = data;
	obj->next = next;
	return (obj);
}

void ft_print_result(int id, bool ok)
{
	printf("Exercice \033[0;33m#0%d\033[0;37m : ", id);
	if (ok)
		printf("\033[0;32mO.K.");
	else
		printf("\033[0;31mK.O.");
	printf("\033[0m\n");
}

int main()
{
	/* Header */
	printf("\033[0;37m----------[\033[0;34mC05\033[0;37m]----------\n\n");
	printf("Developed by \033[0;34mRochB\033[0;37m.\n");
	printf("Version \033[0;34m1.0\n");
	printf("\033[0;34mwww.roch-blondiaux.com\033[0;37m\n\n");

	/* Exercice 0 */
	int zeroData = 1;
	int *zeroPointer = &zeroData;
	t_list *zero = ft_create_elem(zeroPointer);
	ft_print_result(0, zero->data == zeroPointer && zero->next == NULL);

	/* Exercice 1 */
	t_list **begin;
	begin = malloc(15 * sizeof(t_list *));
	begin[2] = ft_create_elem("sun");
	begin[1] = ft_create_obj("moon", begin[2]);
	begin[0] = ft_create_obj("earth", begin[1]);

	ft_list_push_front(begin, "saturn");
	ft_print_result(1, strcmp(begin[0]->data, "saturn") == 0 && strcmp(begin[2]->data, "sun") == 0);

	/* Exercice 2 */
	ft_print_result(2, ft_list_size(*begin) == 4);

	/* Exercice 3 */
	ft_print_result(3, strcmp(ft_list_last(*begin)->data, "sun") == 0);

	/* Exercice 4 */
	ft_list_push_back(begin, "pluton");
	ft_print_result(4, strcmp(ft_list_last(*begin)->data, "pluton") == 0);

	/* Exercice 5 */
	char **fivePointer;
	fivePointer = malloc(sizeof(char *) * 4);
	fivePointer[0] = malloc(sizeof(char) * 3);
	fivePointer[0] = "abc";
	fivePointer[1] = malloc(sizeof(char) * 3);
	fivePointer[1] = "cba";
	fivePointer[2] = malloc(sizeof(char) * 3);
	fivePointer[2] = "daf";
	fivePointer[3] = malloc(sizeof(char) * 3);
	fivePointer[3] = "bge";
	t_list *fivePointerA = ft_list_push_strs(4, fivePointer);
	ft_print_result(5, strcmp(fivePointerA->data, "cba") == 0
		&& strcmp(fivePointerA->next->data, "daf") == 0
		&& strcmp(fivePointerA->next->next->data, "bge") == 0
		&& strcmp(fivePointerA->next->next->next->data, "abc") == 0);

	/* Exercice 6 */
	//ft_list_clear(fivePointerA, free);
	//ft_print_result(6, !fivePointerA);

	/* Exercice 7 */
	ft_print_result(7, strcmp(ft_list_at(fivePointerA, 2)->data, "bge") == 0 && strcmp(ft_list_at(fivePointerA, 3)->data, "abc") == 0);

	/* Exercice 8 */
	ft_list_reverse(&fivePointerA);
	ft_print_result(8, strcmp(ft_list_at(fivePointerA, 2)->data, "daf") == 0 && strcmp(ft_list_at(fivePointerA, 3)->data, "cba") == 0);

	/* Exercice 9 */

	/* Memory */
	free(begin);
	free(fivePointer);

	/* Norminette */
	printf("\n\n\033[0;33mNorminette outputs:\033[0;37m\n\n");
	system("norminette -R CheckForbiddenSourceHeader */*.c");

	/* Footer */
	printf("\n\n----------[\033[0;34mC05\033[0;37m]----------\n");
	return (0);
}