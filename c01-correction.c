#include <stdio.h>
#include <stdlib.h>

#include "ex00/ft_ft.c"
#include "ex01/ft_ultimate_ft.c"
#include "ex02/ft_swap.c"
#include "ex03/ft_div_mod.c"
#include "ex04/ft_ultimate_div_mod.c"
#include "ex05/ft_putstr.c"
#include "ex06/ft_strlen.c"
#include "ex07/ft_rev_int_tab.c"
#include "ex08/ft_sort_int_tab.c"

void exHeader(int id)
{
	printf("Printing exercice #0%d results...\n", id);
}

void exFooter(int id){
	printf("\nExerice #0%d done!\n\n", id);
}

int	main()
{
	int	a;
	int b;

	// HEADER
	printf("----------[C01]----------\n\n");
	printf("Developed by RochB.\n");
	printf("Version 1.0\n");
	printf("www.roch-blondiaux.com\n\n");

	// Exercice1
	exHeader(0);
	printf("Result should be '42':\n");
    a = 12;
    ft_ft(&a);
    printf("Result: %d", a);
	exFooter(0);

	// Exercice2
	exHeader(1);
	printf("Result should be '42':\n");
    
    int n;
	int nbr8;
	int *nbr7;
	int **nbr6;
	int ***nbr5;
	int ****nbr4;
	int *****nbr3;
	int ******nbr2;
	int *******nbr1;
	int ********nbr;

	n = 21;
	
	nbr8 = n;
	nbr7 = &nbr8;
	nbr6 = &nbr7;
	nbr5 = &nbr6;
	nbr4 = &nbr5;
	nbr3 = &nbr4;
	nbr2 = &nbr3;
	nbr1 = &nbr2;
	nbr = &nbr1;
	
    ft_ultimate_ft(&nbr);
    printf("Result: %d", ********nbr);
	exFooter(1);

	// Exercice3
	exHeader(2);
	printf("Result should be '42':\n");
 	a = 2;
    b = 4;

    ft_swap(&a, &b);
    printf("Result: %d%d\n", a, b);
	exFooter(2);

	// Exercice4
	exHeader(3);
	printf("Result should be '2-0':\n");
	int div;
	int mod;

	a = 10;
	b = 5;
	div = 0;
	mod = 0;
	ft_div_mod(a, b, &div, &mod);
	printf("%d-%d", div, mod);
	exFooter(3);
	
	// Exercice5
	exHeader(4);
	printf("Result should be '2-0':\n");

	int *pa;
	int *pb;

	a = 10;
	b = 5;
	pa = &a;
	pb = &b;

	ft_ultimate_div_mod(pa, pb);
	printf("Result: %d-%d", a, b);
	exFooter(4);

	// Exercice6
	exHeader(5);
	printf("Result should be 'Hello world':\n");
	char six[] = {'H' ,'e' ,'l' ,'l' ,'o' ,' ' ,'w' ,'o' ,'r' ,'l' ,'d'};
	ft_putstr(six);
	exFooter(5);
	
	// Exercice7
	exHeader(6);
	printf("Result should be '13':\n");
	char seven[] = "Life is good.";
	int resultseven = ft_strlen(seven);
	printf("Result: %d\n", resultseven);
	exFooter(6);

	// Exercice8
	exHeader(7);
	printf("Result should be '9876543210':\n");
	int eight[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	int eihtSize = 10;

	ft_rev_int_tab(eight, eihtSize);

	a = 0;
	while (a < eihtSize)
	{
		printf("%d", eight[a]);
		a++;
	}
	exFooter(7);

	// Exercice9
	exHeader(8);

	int nine[] = {0, 100, 10, 250, 300, 30, 25, 650, 1};
	int nineSize = 9;

	printf("Result should be '0, 1, 10, 25, 30, 100, 250, 300, 650':\n");
	ft_sort_int_tab(nine, nineSize);
	a = 0;
	while (a < nineSize)
	{
		printf("%d, ", nine[a]);
		a++;
	}
	exFooter(8);

	// NORMINETTE
	printf("Printing norminette results...\n\n");
	system("norminette -R CheckForbiddenSourceHeader */*.c");

	printf("\n\n----------[C01]----------\n");
	return (0);
}


