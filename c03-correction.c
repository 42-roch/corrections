#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "ex00/ft_strcmp.c"
#include "ex01/ft_strncmp.c"
#include "ex02/ft_strcat.c"
#include "ex03/ft_strncat.c"
#include "ex04/ft_strstr.c"
//#include "ex05/ft_strlcat.c"

void printEx(int id, bool ok)
{
	printf("Exercice \033[0;33m#0%d\033[0;37m : ", id);
	if (ok)
		printf("\033[0;32mO.K.");
	else
		printf("\033[0;31mK.O.");
	printf("\033[0m\n");
}

int main()
{
	// HEADER
	printf("\033[0;37m----------[\033[0;34mC04\033[0;37m]----------\n\n");
	printf("Developed by \033[0;34mRochB\033[0;37m.\n");
	printf("Version \033[0;34m1.0\n");
	printf("mmwww.roch-blondiaux.com\033[0;37m\n\n");

	// Exercice 0
	char zeroS1[] = "azertyuiop";
	char zeroS2[] = "azerty";

	printEx(0, strcmp(zeroS1, zeroS2) == ft_strcmp(zeroS1, zeroS2));

	// Exercice 1
	char oneS1[] = "azertyuiop";
	char oneS2[] = "az";
	int oneSize = 4;

	printEx(1, strncmp(oneS1, oneS2, oneSize) == ft_strncmp(oneS1, oneS2, oneSize));

	// Exercice 2
	char twoArray[] = "azertyuiOP124";
	char twoArrayA[50] = "qwd";
	char twoArrayB[50] = "qwd";

	strcat(twoArrayA, twoArray);
	ft_strcat(twoArrayB, twoArray);

	printEx(2, strcmp(twoArrayA, twoArrayB) == 0);

	// Exercice 3
	char threeArray[] = "azertyuiOP124";
	char threeArrayA[10] = "abc";
	char threeArrayB[10] = "abc";
	size_t threeSize = 3;

	strncat(threeArrayA, threeArray, threeSize);
	ft_strncat(threeArrayB, threeArray, threeSize);
	printEx(3, strcmp(threeArrayA, threeArrayB) == 0);

	// Exercice 4
	char fourArray[] = "Hello bud buddy! What's up bud?";
	char fourArrayA[] = "buddy";
	char fourArrayB[] = "";

	printEx(4, strcmp(ft_strstr(fourArray, fourArrayA), strstr(fourArray, fourArrayA)) == 0 
		&& strcmp(ft_strstr(fourArray, fourArrayB), strstr(fourArray, fourArrayB)) == 0);

	// Exercice 5
	/*char fiveArray[] = "azertyuiop121212345";
	char fiveArrayA[] = "nbv901";
	char fiveArrayB[] = "nbv901";
	size_t fiveSize = 3;
	

	printEx(5, strlcat(fiveArrayA, fiveArray, fiveSize) == ft_strlcat(fiveArrayB, fiveArray, fiveSize));
	*/
	// NORMINETTE
	printf("\n\n\033[0;33mNorminette outputs:\033[0;37m\n\n");
	system("norminette -R CheckForbiddenSourceHeader */*.c");

	// FOOTER
	printf("\n\n----------[\033[0;34mC04\033[0;37m]----------\n");
	return (0);
}