#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

void printEx(int id, bool ok)
{
	printf("Exercice \033[0;33m#0%d\033[0;37m : ", id);
	if (ok)
		printf("\033[0;32mO.K.");
	else
		printf("\033[0;31mK.O.");
	printf("\033[0m\n");
}

int main(void)
{
	system("norminette -R CheckForbiddenSourceHeader */*.c");
}