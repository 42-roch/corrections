#include <stdio.h>

#include "ex00/ft_putchar.c"
#include "ex01/ft_print_alphabet.c"
#include "ex02/ft_print_reverse_alphabet.c"
#include "ex03/ft_print_numbers.c"
#include "ex04/ft_is_negative.c"
#include "ex05/ft_print_comb.c"
#include "ex06/ft_print_comb2.c"
#include "ex07/ft_putnbr.c"
#include "ex08/ft_print_combn.c"

void exHeader(int id)
{
	printf("Printing exercice #0%d results...\n", id);
}

void exFooter(int id){
	printf("\nExerice #0%d done!\n\n", id);
}

int	main()
{
	// HEADER
	printf("----------[C00]----------\n\n");

	// Exercice1
	exHeader(0);
	printf("Result should be 'ok':\n");
	ft_putchar('o');
	ft_putchar('k');
	exFooter(0);

	// Exercice2
	exHeader(1);
	printf("The result should be the alphabet:\n");
	ft_print_alphabet();
	exFooter(1);

	// Exercice3
	exHeader(2);
	printf("The result should be the reversed alphabet:\n");
	ft_print_reverse_alphabet();
	exFooter(2);

	// Exercice4
	exHeader(3);
	printf("Result should be '0123456789':\n");
	ft_print_numbers();
	exFooter(3);
	
	// Exercice5
	exHeader(4);
	printf("Result should be '1 -5 0':\n");
	ft_is_negative(1);
	printf(" ");
	ft_is_negative(-5);
	printf(" ");
	ft_is_negative(0);
	exFooter(4);

	// Exercice6
	exHeader(5);
	ft_print_comb();
	exFooter(5);
	
	// Exercice7
	exHeader(6);
	ft_print_comb2();                     
	exHeader(6);

	// Exercice8
	exHeader(7);
	ft_putnbr(42);
	exHeader(7);

	// Exercice9
	exHeader(8);
	ft_print_combn(2); 
	exFooter(8);

	// NORMINETTE
	printf("Printing norminette results...\n\n");
	system("norminette -R CheckForbiddenSourceHeader */*.c");


	// FOOTER
	printf("----------[C00]----------");
	return (0);
}
