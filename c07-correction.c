#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "ex00/ft_strdup.c"
#include "ex01/ft_range.c"
#include "ex02/ft_ultimate_range.c"
#include "ex03/ft_strjoin.c"

void printEx(int id, bool ok)
{
	printf("Exercice \033[0;33m#0%d\033[0;37m : ", id);
	if (ok)
		printf("\033[0;32mO.K.");
	else
		printf("\033[0;31mK.O.");
	printf("\033[0m\n");
}

int main()
{
	// HEADER
	printf("\033[0;37m----------[\033[0;34mC05\033[0;37m]----------\n\n");
	printf("Developed by \033[0;34mRochB\033[0;37m.\n");
	printf("Version \033[0;34m1.0\n");
	printf("\033[0;34mwww.roch-blondiaux.com\033[0;37m\n\n");

	// Exercice 0
	char *zeroArray = "azertyuiop";
	char *zeroArrayA = ft_strdup(zeroArray);

	printEx(0, strcmp(zeroArrayA, zeroArray) == 0 && zeroArrayA[10] == '\0');
	free(zeroArrayA);

	// Exercice 1
	int *oneArray = ft_range(5, -1);
	int *oneArrayA = ft_range(1, 10);
	int *oneArrayB = ft_range(99, 190213);
	printEx(1, oneArray == 0 && oneArrayA[7] == 8 && oneArrayB[10] == 109);
	free(oneArrayA);
	free(oneArrayB);

	// Exercice 2
	int two = 1;
	int *twoA = &two;
	printEx(2, ft_ultimate_range(&twoA, 10, 1) == 0 && ft_ultimate_range(&twoA, 1, 10) == 9);

	// Exercice 3
	char *threeArray = " | ";
	char *threeArrayA = "aaaaa";
	char *threeArrayB = "bbbbb";
	char **threeArrayC = malloc(5000);
	char *threeArrayD = "aaaaa | bbbbb";
	threeArrayC[0] = threeArrayA;
	threeArrayC[1] = threeArrayB;
	printEx(3, strcmp(ft_strjoin(2, threeArrayC, threeArray), threeArrayD) == 0);
	free(threeArrayC);

	// NORMINETTE
	printf("\n\n\033[0;33mNorminette outputs:\033[0;37m\n\n");
	system("norminette -R CheckForbiddenSourceHeader */*.c");

	// FOOTER
	printf("\n\n----------[\033[0;34mC05\033[0;37m]----------\n");
	return (0);
}