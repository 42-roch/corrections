#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "ex00/ft_iterative_factorial.c"
#include "ex01/ft_recursive_factorial.c"
#include "ex02/ft_iterative_power.c"
#include "ex03/ft_recursive_power.c"
#include "ex04/ft_fibonacci.c"
#include "ex05/ft_sqrt.c"
#include "ex06/ft_is_prime.c"
#include "ex07/ft_find_next_prime.c"

void printEx(int id, bool ok)
{
	printf("Exercice \033[0;33m#0%d\033[0;37m : ", id);
	if (ok)
		printf("\033[0;32mO.K.");
	else
		printf("\033[0;31mK.O.");
	printf("\033[0m\n");
}

int main()
{
	// HEADER
	printf("\033[0;37m----------[\033[0;34mC05\033[0;37m]----------\n\n");
	printf("Developed by \033[0;34mRochB\033[0;37m.\n");
	printf("Version \033[0;34m1.0\n");
	printf("\033[0;34mwww.roch-blondiaux.com\033[0;37m\n\n");

	// Exercice 0
	printEx(0, ft_iterative_factorial(4) == 24 && ft_iterative_factorial(5) == 120 && ft_iterative_factorial(3) == 6);

	// Exercice 1
	printEx(1, ft_recursive_factorial(4) == 24 && ft_recursive_factorial(5) == 120 && ft_recursive_factorial(3) == 6);

	// Exercice 2
	printEx(2, pow(3, 5) == ((double)ft_iterative_power(3, 5)) && ft_iterative_power(3, -1) == 0 && ft_iterative_power(3, 0) == 1);

	// Exercice 3
	printEx(3, pow(3, 5) == ((double)ft_recursive_power(3, 5)) && ft_recursive_power(3, -1) == 0 && ft_recursive_power(3, 0) == 1);

	// Exercice 4
	printEx(4, ft_fibonacci(0) == 0 && ft_fibonacci(1) == 1 && ft_fibonacci(-1) == -1 && ft_fibonacci(3) == 2);

	// Exercice 5
	printEx(5, sqrt(25) == (double) ft_sqrt(25) && ft_sqrt(5) == 0);

	// Exercice 6
	printEx(6, ft_is_prime(1) == 0 && ft_is_prime(0) == 0 && ft_is_prime(3) == 1 && ft_is_prime(19) == 1 && ft_is_prime(83) == 1);

	// Exercice 7
	printf("8: %d\n25: %d\n341: %d\n105: %d\n", ft_find_next_prime(2147483646), ft_find_next_prime(25), ft_find_next_prime(341), ft_find_next_prime(105));
	printEx(7, ft_find_next_prime(8) == 11 && ft_find_next_prime(25) == 29 && ft_find_next_prime(341) == 347 && ft_find_next_prime(105) == 107);

	// NORMINETTE
	printf("\n\n\033[0;33mNorminette outputs:\033[0;37m\n\n");
	system("norminette -R CheckForbiddenSourceHeader */*.c");

	// FOOTER
	printf("\n\n----------[\033[0;34mC05\033[0;37m]----------\n");
	return (0);
}
